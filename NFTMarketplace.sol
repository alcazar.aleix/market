// SPDX-License-Identifier: MIT
pragma solidity ^0.8.6;


import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "./Sale.sol";

/**
 * @title NFTMarketplace
 * @dev anyone can buy chests and only TokenOwner can sell their token
 */
contract NFTMarketplace is Ownable, Sale {

    using Address for address;

    /// @notice Information about the sender that placed a bid on an NFT marketplace
    struct marketInfo {
        address seller;
        uint256 tokenCount;
        uint256 sellPrice;
        uint256 sellPriceIndMoon;
        uint256 tokenId;
    }

    /// @notice ERC721 Token ID -> bidder info (if a bid has been received)
    mapping( uint256 => marketInfo ) private sellers;

    uint256 private _currentId;

    /// @notice total NFT count
    uint256 private _totalCount;

    /// @notice rarity for each NFT token
    mapping( uint256 => uint256 ) private rarity;

    /// @notice address of admin wallet
    address adminWallet;

    // @notice ERC20 DevilMoon token
    IERC20 public devilMoon;



    modifier onlyRNG() {
        require(
            msg.sender == address(RNG),
            "NFTMarketplace: Caller is not the RandomNumberGenerator"
        );
        _;
    }

    modifier onlyTokenOwner(uint256 _tokenId, uint256 _tokenCount ) {
        require( balanceOf(msg.sender, _tokenId) >= _tokenCount, "has not enough token");
        _;
    }

    modifier onlyTransactionOwner( uint256 _marketId ) {
        require( sellers[_marketId].seller == msg.sender, "only this transaction's owner can call this function");
        _;
    }

    /**
     * @notice Sale Constructor
     * @param _hmoon hmoon Interface
     * @param _dmoon devilMoon Interface
     * @param uri default uri
     * @param startPresaleDate first presale starting time
     * @param firstduration first presale duration
     * @param secondPresaleDate second presale starting time
     * @param secondDuration second presale duration
     * @param startPublicsaleDate public sale starting time
     * @param _RNG random number generator interface
     */
    constructor(IERC20 _hmoon, IERC20 _dmoon, string memory uri,uint256 startPresaleDate, uint256 firstduration, uint256 secondPresaleDate, 
                uint256 secondDuration, uint256 startPublicsaleDate, IRandomNumberGenerator _RNG) 
                Sale(_hmoon, uri, startPresaleDate, firstduration, secondPresaleDate, secondDuration, startPublicsaleDate, _RNG)  {

                    devilMoon = _dmoon;
                    _totalCount = 12;

                    //set rarity
                    rarity[0] = 8;
                    rarity[1] = 8;
                    rarity[3] = 8;
                    rarity[4] = 8;
                    rarity[5] = 8;
                    rarity[6] = 8;
                    rarity[7] = 8;
                    rarity[8] = 8;
                    rarity[9] = 8;
                    rarity[10] = 8;
                    rarity[11] = 12;

    }

  
    /**
     * @notice get tokenId based random number
     * @param _random random number from chain link
    */
    function getRealNumber( uint256 _random) internal returns(uint256) {
        uint256 randomNum = _random % 100 + 1;

        uint256 percent = 0;
        uint256 i = 0;
        for( i = 0; i < _totalCount ; i++ ) {
            percent += rarity[i];
            if( randomNum <= percent ) {
                break;
            }
        }
        require( i < _totalCount, "cannot buy item");
        increaseUsedCount();
        return i;
    }

    /**
     * @notice get hash number based random number
     * @param _random random number from chain link
    */
    function getHashNumber( uint256 _random ) internal view returns(uint256) {
        return uint(keccak256(abi.encodePacked(block.difficulty, block.timestamp, _random)));
    }

    /**
     * @notice sale NFT function
     * @dev Only Random number generator contract can call this function
     * @param _requestId requestID from chain link to get the random number
     * @param _randomness random number from chain link
    */
    function sale(bytes32 _requestId, uint256 _randomness) external onlyRNG {
        SaleInfo storage saleInfo = requestToSale[_requestId];
        uint256 itemIdx = getRealNumber(_randomness);

        

        
        uint256 hmoon_price = saleInfo.priceInhmoon;

        require( hmoon.balanceOf(saleInfo.buyer) >= hmoon_price, "you have not enough hmoon");
        hmoon.approve(saleInfo.buyer, hmoon_price);
        hmoon.transferFrom(saleInfo.buyer, adminWallet, hmoon_price);

        super._mint( saleInfo.buyer, itemIdx, 1, "");
        for( uint i = 0; i < saleInfo.chestType; i++ ) {
            itemIdx = getRealNumber( getHashNumber(_randomness) );
            super._mint( saleInfo.buyer, itemIdx, 1, "");
        }
        emit SaleFinished( saleInfo.buyer, saleInfo.chestType);
    }

    /**
     * @notice buy NFT token
     * @param _marketId market ID of the token to buy
    */
    function buyToken( uint256 _marketId ) external {

        marketInfo storage sellInfo = sellers[_marketId];
        uint256 tokenCount = sellInfo.tokenCount;
        uint256 tokenId = sellInfo.tokenId;
        address ownerAddr = sellInfo.seller;
        require( tokenCount > 0, "token count must be larger than 0");
        require(balanceOf(address(this), tokenId) > tokenCount,"smart contract haven't got enough token");
        require(sellInfo.sellPrice > 0 ,"price cannot be zero");

        uint256 dmoon_price = sellInfo.sellPriceIndMoon;

        //Get hmoon price
        require( devilMoon.balanceOf(msg.sender) >= dmoon_price, "you have not enough hmoon");

        uint256 fee = dmoon_price / 100 * 8;
        dmoon_price = dmoon_price - fee;

        devilMoon.approve(msg.sender, dmoon_price);
        devilMoon.transferFrom(msg.sender, ownerAddr, dmoon_price);

        devilMoon.approve(msg.sender, fee);
        devilMoon.transferFrom(msg.sender, adminWallet, fee);

        safeTransferFrom( address(this), msg.sender, tokenId, tokenCount, "");

        delete sellers[_marketId];
    }

    /**
     * @notice add token Request
     * @param _tokenId Token ID of the token to sold
     * @param _tokenCount token count to sold
     * @param _price set price of token in USD
     * @param _dmoon_count set price of token in dmoon
    */
    function addRequest( uint256 _tokenId, uint256 _tokenCount, uint256 _price, uint256 _dmoon_count ) external onlyTokenOwner(_tokenId,_tokenCount) returns(uint256) {
        require( _price > 0, "set price must be larger than 0$");
        require( _tokenCount > 0, "token count must be larger than 0");
        marketInfo storage sellInfo = sellers[_currentId];
        sellInfo.seller = msg.sender;
        sellInfo.tokenCount = _tokenCount;
        sellInfo.sellPrice = _price;
        sellInfo.sellPriceIndMoon = _dmoon_count;
        sellInfo.tokenId = _tokenId;

        safeTransferFrom( msg.sender, address(this), _tokenId, _tokenCount, "");

        _currentId++;
        return _currentId;
    }

    /**
     * @notice change token Request
     * @param _marketId market ID of the token to change
     * @param _tokenCount token count to sold
     * @param _price set price of token in USD
     * @param _dmoon_count set price of token in dmoon
    */
    function changeRequest( uint256 _marketId, uint256 _tokenCount, uint256 _price, uint256 _dmoon_count ) external onlyTransactionOwner(_marketId) {
        require( _price > 0, "set price must be larger than 0$");
        require( _tokenCount > 0, "token count must be larger than 0");
        marketInfo storage sellInfo = sellers[_marketId];        

        uint256 addedCount = 0;
        if( sellInfo.tokenCount > _tokenCount) addedCount = _tokenCount - sellInfo.tokenCount;
        require( balanceOf(msg.sender, sellInfo.tokenId) >= addedCount, "not enough token in your wallet");

        if( addedCount > 0 ) {
            safeTransferFrom( msg.sender, address(this), sellInfo.tokenId, addedCount, "");
        }
        else {
            addedCount = sellInfo.tokenCount - _tokenCount;
            safeTransferFrom( address(this), msg.sender, sellInfo.tokenId, addedCount, "");
        }
        sellInfo.seller = msg.sender;
        sellInfo.tokenCount = _tokenCount;
        sellInfo.sellPrice = _price;
        sellInfo.sellPriceIndMoon = _dmoon_count;
    }

    /**
     * @notice delete token Request
     * @param _marketId market ID of the token to delete
    */
    function deleteRequest( uint256 _marketId ) external onlyTransactionOwner(_marketId) {
        marketInfo storage sellInfo = sellers[_marketId]; 
        require( sellInfo.sellPrice > 0 ,"price must be larger than 0");
        require( sellInfo.tokenCount > 0 ,"tokenCount be larger than 0");
        safeTransferFrom( address(this), msg.sender, sellInfo.tokenId, sellInfo.tokenCount, "");
        delete sellers[_marketId];
    }


    /**
     * @notice get token price in USD
     * @param _marketId market ID of the token to get price
    */
    function getMarketInfo( uint256 _marketId ) external view returns(marketInfo memory) {
        marketInfo memory sellInfo = sellers[_marketId];
        require( sellInfo.seller != address(0), "Not in marketplace");
        require( sellInfo.sellPrice > 0, "Not in marketplace");
        require( sellInfo.sellPriceIndMoon > 0, "Not in marketplace");
        return sellInfo;
    }

    /**
     * @notice get token owner
     * @param _tokenId Token ID of the token to get owner
    */
    function getTokenCount( uint256 _tokenId ) external view returns(uint256) {
        return balanceOf( msg.sender, _tokenId);
    }

    /**
     * @notice set token rarity
     * @param _totalRarityCount set total rarity count
	 * @param _rarity[] each token's rarity
    */	
	function setRarity( uint256 _totalRarityCount, uint256[] memory _rarity ) external onlyOwner {
		require( _rarity.length == _totalRarityCount, "total rarity count and rarity array does not match");
		uint sum = 0;
		for( uint i = 0; i < _rarity.length; i++ ) {
			sum += _rarity[i];
		}
		require( sum == 100,"total rarity must be 100");
		_totalCount = _totalRarityCount;

        for( uint i = 0; i < _rarity.length; i++ ) {
            rarity[i] = _rarity[i];
        }
	}

}